<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tentukan Nilai</title>
</head>
<body>
    <h1>Tentukan Nilai PHP</h1>
    <?php 

        function tentukan_nilai($number) {
            if ($number >= 85 && $number <= 100) {
                echo "$number = Sangat Baik<br>";
            } elseif ($number >= 70 && $number < 85) {
                echo "$number = Baik<br>";
            } elseif ($number >= 60 && $number < 70) {
                echo "$number = Cukup<br>";
            } else {
                echo "$number = Kurang<br>";
            }
        }

        //TEST CASES
        echo tentukan_nilai(98); //Sangat Baik
        echo tentukan_nilai(76); //Baik
        echo tentukan_nilai(67); //Cukup
        echo tentukan_nilai(43); //Kurang
        
    ?>
</body>
</html>